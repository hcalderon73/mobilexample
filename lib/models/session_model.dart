class Session {
  String _username;
  String _url;
  String _accessToken;
  String _errorDescription;

  Session(this._username, this._accessToken, this._url, this._errorDescription);

  Session.map(dynamic obj) {
    this._username = obj["username"];
    this._accessToken = obj["password"];
    this._url = obj["url"];
    this._errorDescription = obj["errorDescription"];
  }

  String get username => _username;
  String get accessToken => _accessToken;
  String get url => _url;
  String get errorDescription => _errorDescription;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = _username;
    map["accessToken"] = _accessToken;
    map["url"] = _url;
    map["errorDescription"] = _errorDescription;

    return map;
  }
}

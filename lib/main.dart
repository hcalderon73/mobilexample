import 'package:flutter/material.dart';
import 'package:mobile_e/router/route.dart';
import 'package:mobile_e/services/session.dart';
import 'blocs/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final prefs = new SessionMobile();
  await prefs.initPrefs();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final sessionMobile = SessionMobile();

  @override
  Widget build(BuildContext context) {
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: (sessionMobile.accessToken != null) ? '/envios' : '/login',
        onGenerateRoute: Router.generateRoute,
      ),
    );
  }
}

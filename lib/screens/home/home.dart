import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  static const routeName = '/home';

  final String title;
  final String message;

  const Home({Key key, @optionalTypeArgs this.title, @optionalTypeArgs this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Home'),
        ),
        body: Center(
          child: Container(
            child: Column(
              children: <Widget>[
                Text('Luxusedge $title'),
                MaterialButton(
                    child: Text('Envios'),
                    onPressed: () => Navigator.pushNamed(context, '/envios', arguments: {'title': 'Envios', 'message': 'PAGE'}))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

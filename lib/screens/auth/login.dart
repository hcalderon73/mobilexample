import 'package:flutter/material.dart';
import 'package:mobile_e/blocs/provider.dart';
import 'package:mobile_e/services/auth.dart';
import 'package:mobile_e/services/session.dart';

class Login extends StatelessWidget {
  static const routeName = '/login';
  const Login({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    

    return Scaffold(
      body: Stack(
        children: <Widget>[
          _createBackground(context),
          _logo(),
          _loginForm(context)
        ],
      ),
    );
  }

  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 185.0,
            ),
          ),
          Container(
            width: size.width * 0.8,
            padding: EdgeInsets.symmetric(vertical: 40.0),
            margin: EdgeInsets.symmetric(vertical: 20.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(3.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 1.0),
                  )
                ]),
            child: Column(
              children: <Widget>[
                // INPUT URL.
                _inputUrl(bloc),

                // INPUT USERNAME.
                _inputUsername(bloc),

                // INPUT PASSWORD.
                _inputPassword(bloc),
                SizedBox(
                  height: 10.0,
                ),
                // SUBMIT.
                _submit(bloc),
              ],
            ),
          )
        ],
      ),
    );
  }

  // FORM INPUTS.
  Widget _inputUrl(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.urlStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.url,
            decoration: InputDecoration(
                labelText: 'Url',
                hintText: 'Url',
                errorText: snapshot.error,
                icon: Icon(Icons.unfold_more, color: Colors.deepPurple)),
            onChanged: bloc.changeUrl,
          ),
        );
      },
    );
  }

  // FORM INPUTS.
  Widget _inputUsername(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.usernameStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                labelText: 'Usuario',
                hintText: 'Usuario',
                errorText: snapshot.error,
                icon: Icon(Icons.alternate_email, color: Colors.deepPurple)),
            onChanged: bloc.changeUsername,
          ),
        );
      },
    );
  }

  // FORM INPUTS.
  Widget _inputPassword(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
          child: TextField(
            obscureText: true,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                labelText: 'Contraseña',
                hintText: 'Contraseña',
                errorText: snapshot.error,
                icon: Icon(Icons.lock_outline, color: Colors.deepPurple)),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  // SUBMIT.
  Widget _submit(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
          color: Colors.deepPurple,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 0.0,
          textColor: Colors.white,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 100.0, vertical: 15.0),
            child: Text(
              'Ingresar',
            ),
          ),
          onPressed: (snapshot.hasData) ? () => _login(bloc, context) : null,
        );
      },
    );
  }

  void _login(LoginBloc bloc, BuildContext context) async {
    final url = bloc.url;
    final username = bloc.username;
    final password = bloc.password;
    // pasamos estos datos al metodo Login.
    final api = Auth();
    await api.login(username, password, url);

    final sessionMobile = SessionMobile();
    // OK
    if (sessionMobile.accessToken != "") {
      Navigator.pushReplacementNamed(context, '/envios');
    }

    // ERROR
    if (sessionMobile.errorDescription != "") {
      _showToast(context, sessionMobile.errorDescription);
    }
  }

  void _showToast(BuildContext context, String errorDescription) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(errorDescription),
        action: SnackBarAction(
            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  Widget _logo() {
    return Container(
      padding: EdgeInsets.only(top: 80.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
          ),
          Container(
            child: Icon(
              Icons.cast,
              size: 80,
              color: Colors.white,
            ),
          ),
          Text(
            'Canela',
            style: TextStyle(color: Colors.white, fontSize: 30.0),
          )
        ],
      ),
    );
  }

  Widget _createBackground(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: <Color>[
          Color.fromRGBO(63, 63, 156, 1.0),
          Color.fromRGBO(90, 70, 178, 1.0),
        ]),
      ),
      child: Text('d'),
    );
  }
}

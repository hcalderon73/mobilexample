import 'package:flutter/material.dart';

class Albaranes extends StatelessWidget {
  static const routeName = '/albaranes';

  final String title;
  final String message;

  const Albaranes(
      {Key key, @optionalTypeArgs this.title, @optionalTypeArgs this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          
          appBar: AppBar(
            
            title: Text('Tabs Demo'),
          ),
          body: TabBarView(
            children: [
              Icon(Icons.directions_car),
              Icon(Icons.directions_transit),
              Icon(Icons.directions_bike),
            ],
          ),
          bottomNavigationBar: BottomAppBar(
            color: Colors.red,
            elevation: 200.0,
            child: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.directions_car)),
                Tab(icon: Icon(Icons.directions_transit)),
                Tab(icon: Icon(Icons.directions_bike)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:mobile_e/services/session.dart';

class Envios extends StatelessWidget {
  static const routeName = '/envios';

  final String title;
  final String message;

  const Envios(
      {Key key, @optionalTypeArgs this.title, @optionalTypeArgs this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sessionMobile = SessionMobile();
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Envios'),
        ),
        body: Center(
          child: Container(
            child: Column(
              children: <Widget>[
                Text('Luxusedge $title'),
                Text('Luxusedge ${sessionMobile.accessToken}'),
                Text('Luxusedge ${sessionMobile.url}'),
                MaterialButton(
                    child: Text('Home'),
                    onPressed: () => Navigator.pushNamed(context, '/albaranes',
                        arguments: {'title': 'Home !!!', 'message': 'PAGE'}))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

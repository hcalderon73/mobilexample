import 'package:flutter/material.dart';
import 'package:mobile_e/screens/albaranes/albaranes.dart';
import 'package:mobile_e/screens/auth/login.dart';
import 'package:mobile_e/screens/envios/Envios.dart';
import 'package:mobile_e/screens/home/home.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final Map args = settings.arguments;


    switch (settings.name) {
      case Home.routeName:
        return MaterialPageRoute(
          builder: (context) {
            return Home(
              title: (args != null ) ? args['title'] : 'Home',
              message: (args != null ) ? args['message'] : 'PAGE',
            );
          },
        );
        break;
      case Envios.routeName:
        return MaterialPageRoute(
          builder: (context) {
            return Envios(
              title: (args != null ) ? args['title'] : 'Envios',
              message: (args != null ) ? args['message'] : 'PAGE',
            );
          },
        );
        break;
      case Albaranes.routeName:
        return MaterialPageRoute(
          builder: (context) {
            return Albaranes(
              title: args['title'],
              message: args['message'],
            );
          },
        );
        break;
      case Login.routeName:
        return MaterialPageRoute(
          builder: (context) {
            return Login();
          },
        );
        break;
      default:
    }
  }
}

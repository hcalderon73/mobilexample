import 'dart:async';

import 'package:mobile_e/models/session_model.dart';
import 'package:mobile_e/services/network.dart';
import 'package:mobile_e/services/session.dart';

class Auth {
  Network _networkService = new Network();
  final _prefs = new SessionMobile();

  get errorDescription => null;

  Future<Session> login(String username, String password, String url) {
    final baseUrl = url;
    final loginUrl = baseUrl + '/token';
    final data =
        "grant_type=password&username=$username&password=$password&client_id=MobileS";
    final headers = {"Content-Type": "application/json"};

    return _networkService
        .post(loginUrl, body: data, headers: headers)
        .then((dynamic res) {
      _prefs.url = url;
      _prefs.username = username;
      _prefs.accessToken = res['access_token'];
      _prefs.errorDescription = res["error_description"];
      /*if (res["error"] != null) {
       throw new Exception(res["error_description"]);
      }*/
      
      return new Session.map({
        username: username,
        url: url,
        password: res['access_token'],
        errorDescription: res["error_description"],
      });
      
    });
  }
}


import 'package:shared_preferences/shared_preferences.dart';

class SessionMobile {
  static final SessionMobile _instancia = new SessionMobile._internal();

  factory SessionMobile() {
    return _instancia;
  }

  SessionMobile._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  get url {
    return _prefs.getString('url') ?? '';
  }

  set url(String value) {
    _prefs.setString('url', value);
  }

  get username {
    return _prefs.getString('username') ?? '';
  }

  set username(String value) {
    _prefs.setString('username', value);
  }

  get accessToken {
    return _prefs.getString('accessToken') ?? '';
  }

  set accessToken(String value) {
    _prefs.setString('accessToken', value);
  }

  get errorDescription {
    return _prefs.getString('errorDescription') ?? '';
  }

  set errorDescription(String value) {
    _prefs.setString('errorDescription', value);
  }
}

import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:mobile_e/blocs/validator.dart';

class LoginBloc with Validators {

  final _urlController      = BehaviorSubject<String>();
  final _usernameController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  Stream<String> get urlStream      => _urlController.stream.transform(validarUrl);
  Stream<String> get usernameStream => _usernameController.stream.transform(validarUsername);
  Stream<String> get passwordStream => _passwordController.stream.transform(validarPassword);

  Stream<bool> get formValidStream  => CombineLatestStream.combine3(
    urlStream,
    usernameStream,
    passwordStream,

    (a, b, c) => true,
  );

  String get url      => _urlController.value;
  String get username => _usernameController.value;
  String get password => _passwordController.value;



  // Insertar valores.
  Function(String) get changeUrl      => _urlController.sink.add;
  Function(String) get changeUsername => _usernameController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;

  dispouse() {
    _urlController?.close();
    _usernameController?.close();
    _passwordController?.close();
  }
}

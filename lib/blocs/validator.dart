

import 'dart:async';

class Validators {


  //  ^((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)
  //  EMAIL: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/



  final validarUrl = StreamTransformer<String, String>.fromHandlers(
    handleData: (url, sink) {
      
      Pattern pattern = r"(https?|ftp)://([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:‌​,.;]*)?";
      RegExp regExp = new RegExp(pattern, caseSensitive: false);

      if(regExp.hasMatch(url)) {
        sink.add(url);
      } else {
        sink.addError('Url valida');
      }
    }
  );


  final validarUsername = StreamTransformer<String, String>.fromHandlers(
    handleData: (username, sink) {
      if(username.length >= 4) {
        sink.add(username);
      } else {
        sink.addError('Superior a 4 caracteres');
      }
    }
  );
  
  
  final validarPassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (password, sink) {
      if(password.length >= 4) {
        sink.add(password);
      } else {
        sink.addError('Superior a 4 caracteres');
      }
    }
  );
}
